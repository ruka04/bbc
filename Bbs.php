<?php

require_once __DIR__ . '/BbsData.php';

class Bbs
{
    // データクラスインスタンス
    protected $data;
    
    // コンストラクタ
    public function __construct($dataPath)
    {
        $this->data = new BbsData($dataPath);
    }
    
    // 記事データ取得
    public function getThreadsData()
    {
        $data = $this->data->getData();
        
        // 親記事配列生成
        $parents = array();
        foreach ($data as $row) {
            // 親記事番号が0のデータが親記事
            if (0 == $row[BbsData::COL_PARENT_NO]) {
                $parents[] = $row;
            }
        }
        
        // 親記事と子記事の階層を生成
        foreach ($parents as $idx => $parent) {
            $childs = array();
            foreach ($data as $row) {
                // 親記事自身の記事番号と一致する親記事番号を持つものがレス記事データ
                if ($parent[BbsData::NO] == $row[BbsData::COL_PARENT_NO]) {
                    $childs[] = $row;
                }
            }
            $parents[$idx]['Childs'] = $childs;
        }
        
        // 記事配列を逆転して新しい記事順に並べる
        $parents = array_reverse($parents);
        
        return $parents;
    }
    
    // 記事書き込み
    public function write($post)
    {
        // 書き込み用データ生成
        $data[BbsData::COL_NO]  = $this->dac->getNewNo();
        $data[BbsData::COL_PARENT_NO] = $post['parent_no'];
        $data[BbsData::COL_NAME] = $post['name'];
        $data[BbsData::COL_TITLE] = $post['title'];
        $data[BbsData::COL_MESSAGE] = $post['message'];
        
        // 書き込み実行
        $this->data->write($data);
    }
    
    // 記事削除
    public function delete($post)
    {
        $no = $post['no'];
        $this->data->delete($no);
    }
}

?>