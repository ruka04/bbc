<?php

class BbsData
{
    // まずはデータクラスの実装です。

    // 今回の掲示板では、データはテキストファイルとして保管し、そのデータフォーマットはCSVとします。
    // で、データとして必要な項目を考えた場合、以下の5項目くらいは必要になります。

    // クラス定数の設定
    // 投稿番号（投稿があるごとに設定される連番）
    const COL_NO = 0;
    // 親番号（新規投稿の場合は0、返信の場合はその親となる記事の投稿番号）
    const COL_PARENT_NO = 1;
    // 投稿者名
    const COL_NAME = 2;
    // タイトル
    const COL_TITLE = 3;
    // 本文
    const COL_MESSAGE = 4;

    // データファイルパス
    protected $_dataPath;
    
    // コンストラクタ
    public function __construct($dataPath)
    {
        if (false == file_exists($dataPath)) {
            if (false == @touch($dataPath)) {
                throw new Exception('データファイルの作成に失敗しました');
            }
        }
        $this->_dataPath = $dataPath;
    }
    
    // データ取得
    public function getData()
    {
        $fp = fopen($this->_dataPath, 'r');
        $data = array();
        while ($row = fgetcsv($fp)) {
            $data[] = $row;
        }
        fclose($fp);
        
        $data = array_reverse($data);
        return $data;
    }

    // 登録用記事番号取得
    public function getNewNo()
    {
        $data = $this->getData();
        $no = 1;
        if (0 < count($data)) {
            $no = $data[0][self::COL_NO];
            $no++;
        }
        return $no;
    }
 
    // 書き込み
    public function append($data)
    {
        $this->sanitize($data);
        $line = implode(',', $data) . "\r\n";
        
        $fp = fopen($this->_dataPath, 'a');
        fputs($fp, $line);
        fclose($fp);
    }

    // 書き込みデータ整形
    protected function sanitize(&$data)
    {
        foreach ($data as $index => $val) {
            $tmpData = $val;
            $tmpData = trim($tmpData);
            $tmpData = str_replace(',', '', $tmpData);
            if (self::COL_MESSAGE == $index) {
                $tmpData = nl2br($tmpData);
            }
            $tmpData = str_replace("\r", '', $tmpData);
            $tmpData = str_replace("\n", '', $tmpData);
            $data[$index] = $tmpData;
        }
    }
    
    // 削除
    public function delete($no)
    {
        $data = $this->getData();
        
        $writeData = array();
        if (0 < count($data)) {
            foreach ($data as $row) {
                if ($row[self::COL_NO] != $no && $row[self::COL_PARENT_NO] != $no) {
                    $writeData[] = implode(',', $row);
                }
            }
        }
        $writeText = '';
        if (0 < count($writeData)) {
            $writeText = implode("\r\n", $writeData) . "\r\n";
        }

        $fp = fopen($this->_dataPath, 'w');
        fputs($fp, $writeText);
        fclose($fp);
    }
}

?>