<?php

require_once './classes/bbs/Bbs.php';

try {
    $bbs = new Bbs('./data/bbs.dat');

    $parentNo = 0;
    $message = '';

    // 返信ボタンが押された場合
    if (true == isset($_POST['res'])) {
        // 返信投稿モードであることを示す文言
        $parentNo = $_POST['parent_no'];
        $message = sprintf('No.%sへの返信です', $parentNo);
    }    
    // 書き込みボタンが押された場合
    if (true == isset($_POST['write'])) {
        $bbs->write($_POST);
        header('Location: ' . $_SERVER['SCRIPT_NAME']);
        exit;
    }
    // 削除ボタンが押された場合
    if (true == isset($_POST['del'])) {
        $bbs->delete($_POST);
        header('Location: ' . $_SERVER['SCRIPT_NAME']);
        exit;
    }

    // 記事データ取得
    $data = $bbs->getThreadData();

} catch(Exception $e) {
    die($e->getMessage());
}

?>
<html>
<head>
<title>掲示板</title>
</head>
<body>

<form method="post" action="">
<input type="text" name="name" /><br />
<input type="text" name="title" /><br />
<textarea name="message">